# Usage
Right click any .ts file and choose Generate Spec to create a spec.ts file in the same directory with some boilerplate. If a spec.ts file already exists for the .ts file, it will be overwritten with the new one.

# Installation
1.) Go to https://bitbucket.org/energycap/specgen/downloads/ and download `specgen-1.0.0.vsix`  
2.) In VSCode press `Ctrl+Shift+P` to open the Command Pallete and search `Extensions: Install from VSIX`. Find the .vsix file you just downloaded.

# Development
Debugging: Click `Run -> Start Debugging (F5)` in VSCode  
Running Tests: `npm run test`  
Adding Tests: See tests.ts  

# Packaging
1.) Install vsce `npm install -g vsce`  
2.) At project root run `vsce package`. This will create the .vsix file, which can be installed by users.  