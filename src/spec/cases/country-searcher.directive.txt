import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { AfterViewInit, Directive, Host, Injector, OnInit } from '@angular/core';
import { ComboboxComponent, MenuItem } from '@energycap/components';
import { AdminService, Country, CountryService, SystemSettingsResponse } from '@energycap/energycap-sdk-angular';
import { CountryHelperService } from 'src-ui/app/core/country-helper.service';
import { SearcherBaseDirective } from '../searcher-base.directive';

@Directive({
  selector: '[ecCountrySearcher]'
})
export class CountrySearcherDirective extends SearcherBaseDirective {

  /** List of all possible countries */
  private countries?: Country[];

  /** Alpha-2 default country code from the SystemSettingsResponse */
  private defaultCountryCode?: string;

  constructor(
    @Host() protected combobox: ComboboxComponent,
    protected countryHelperService: CountryHelperService,
    protected adminService: AdminService,
    protected injector: Injector
  ) { super(combobox, injector) }

  protected async getApiResponse(searchTerm?: string): Promise<HttpResponse<Country[]>> {
    await this.getData();
    const filteredCountries = this.countries!.filter(c => c.name!.toLowerCase().includes(searchTerm?.toLowerCase() ?? ''));
    return Promise.resolve(new HttpResponse({ body: filteredCountries }));
  }

  protected formatMenuItems(countries: Country[]):  MenuItem<Country>[] {
    const menuItems = countries.map(c => this.countryHelperService.countryToMenuItem(c));
    return this.addDividers(menuItems);
  }

  /** Get the countries and defaultCountryCode from the API and cache them for later searches */
  private async getData() {
    if(!this.countries && !this.defaultCountryCode) {
      const countriesPromise = this.countryHelperService.getCountries();
      const systemSettingsPromise = this.adminService.getSystemSettingsNew().toPromise();
      this.countries = await countriesPromise;
      this.defaultCountryCode = (await systemSettingsPromise).defaultCountry!;
      this.unshiftDefaultCountry();
    }
  }

  /** Move the default country to the front of the countries list */
  private unshiftDefaultCountry() {
    //If the default country isn't configured, do nothing since OTHER isn't going to be in the list of countries
    if(this.defaultCountryCode !== 'OTHER') {
      const defaultCountry = this.countries!.find(c => c.alpha2Code === this.defaultCountryCode)!;
      this.countries = this.countries!.filter(c => c !== defaultCountry);
      this.countries.unshift(defaultCountry);
    }
  }

  /**
   * A divider should be added to the last of US, CA, and the default country.
   * We could speed this up by only checking the first 3 entries of menuItems since US, CA, and defaultCountry will
   * always be one of those if present, but since there are only 250 countries, the difference won't be noticeable. 
   */
  private addDividers(menuItems: MenuItem<Country>[]): MenuItem<Country>[] {
    const US = menuItems.findIndex(item => item.value!.alpha2Code === 'US');
    const CA = menuItems.findIndex(item => item.value!.alpha2Code === 'CA');
    const defaultCountry = menuItems.findIndex(item => item.value!.alpha2Code === this.defaultCountryCode!);
    let lastIndex = Math.max(US, CA, defaultCountry);
    if(lastIndex > -1) {
      menuItems[lastIndex].display = 'divider';
    }
    return menuItems;
  }
}
