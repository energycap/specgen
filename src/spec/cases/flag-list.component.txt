import { Component, Injector, ViewChild } from '@angular/core';
import { PagerSettings, PageChangeEvent, SelectionEvent, GridDataResult, GridComponent, ColumnBase, ColumnComponent } from '@progress/kendo-angular-grid';
import { ListService, DynamicListRequest, ListResponse, UserResponse, ListResponseListField, ListColumnEdit, FilterResponse, FlagTypeChild, QuickFilter, Me } from '@energycap/energycap-sdk-angular';
import { camelCase } from 'lodash';
import { MenuItem, Overlay, DialogService, RadioButtonOption, WindowService, ErrorService } from '@energycap/components';
import { FormGroup, FormControl } from '@angular/forms';
import { BillFlagListFiltersComponent } from '../../custom/flag-list-filters/bill-flag-list-filters.component';
import { Observable, Subscription } from 'rxjs';
import { tap, debounceTime, refCount, publishReplay, takeUntil, filter } from 'rxjs/operators';
import { FlagActionEntities, ManageFlagDialogContext } from '../../../manage/flag/manage-flag-dialog-context';
import { BillFlagFilters, Filter } from '../../custom/flag-list-filters/bill-flag-filters';
import { FlagListFiltersContext } from '../../custom/flag-list-filters/flag-list-filters-context';
import { FlagListFilterService, QuickFiltersWithError } from '../../../core/flag-list-filter.service';
import { SortDescriptor } from '@progress/kendo-data-query';
import { HttpHeaders } from '@angular/common/http';
import { ViewBaseComponent } from '../../../core/view-base.component';
import { TranslateService } from '@ngx-translate/core';
import { BillViewPagerService } from '../../../bills/bill-view/bill-view-pager/bill-view-pager.service';
import { FlagHelperService } from '../../../core/flag-helper.service';
import { FlagListDownloadEntities } from '../../custom/flag/flag-list-download/flag-list-download.component';
import { Tag } from '../tags/tag';

export interface FlagFiltersFormValue {
  flagStatus: boolean,
  flagType: MenuItem<FlagTypeChild> | null,
  costRecovery: {
    operator: string | null | undefined,
    value: number | null;
  },
  assignee: MenuItem<UserResponse | string> | null
}

export class FlagFiltersFormGroup extends FormGroup {
  public value!: FlagFiltersFormValue;

  private defaultNumberOperator: RadioButtonOption<string>;

  constructor(defaultNumberOperator: RadioButtonOption<string>) {
    super({
      flagStatus: new FormControl(),
      flagType: new FormControl(),
      costRecovery: new FormGroup({
        operator: new FormControl(),
        value: new FormControl()
      }),
      assignee: new FormControl()
    });
    this.defaultNumberOperator = defaultNumberOperator;
    this.patchValue(this.getDefaultValue(), {emitEvent: false})
  }

  reset(options?: {onlySelf?: boolean, emitEvent?: boolean}) {
    super.reset(this.getDefaultValue(), options);
  }

  public getDefaultValue(): FlagFiltersFormValue {
    return {
      flagStatus: false,
      flagType: null,
      costRecovery: { operator: this.defaultNumberOperator.value, value: null },
      assignee: null
    }
  }
}

interface FlagRow {
  [key: string]: any
}

export interface FlagFilters {
  flagStatus: Filter;
  flagType: Filter;
  costRecovery: Filter;
  assignees: Filter;
}

@Component({
  selector: 'ec-flag-list',
  templateUrl: './flag-list.component.html',
  styleUrls: ['../tags/tags.component.scss','./flag-list.component.scss']
})
export class FlagListComponent extends ViewBaseComponent {

  /** Object bound to the kendo grid to display the rows */
  public gridData!: GridDataResult;

  /** Used as a map to get details easily for fields once their captions
   *  have been camelCased. Can be used in both template and ts
   */
  public columnDetails: { [key: string]: ListResponseListField } = {};

  /** Tells the kendo grid to show the pager controls or not */
  public pageable: PagerSettings | boolean = false;

  /** Kendo grid page size */
  public pageSize: number = 50;

  /** Used to determine what page the user is on in the grid */
  public skip: number = 0;

  /** Sort columns used for the kendo grid */
  public sort: SortDescriptor[] = [];

  /** Used to filter the grid by resolved or unresolved flags */
  public flagStatusOptions: RadioButtonOption[] = [
    {label: 'Resolved_TC', value: true, tooltip: 'Resolved_TC'}, {label: 'Unresolved_TC', value: false, tooltip: 'Unresolved_TC'}
  ];

  /** Flag types used for the filter control */
  public flagTypeOptions: MenuItem[] = [];

  /** Operators allowed for number type fields, used for the Cost Recovery filter */
  public costRecoveryOperators: RadioButtonOption<string>[] = this.flagListFilterService.getNumberOperators('CostRecovery');

  /** Form group used for the filter controls */
  public formGroup: FlagFiltersFormGroup = new FlagFiltersFormGroup(this.costRecoveryOperators[0]);

  /** Assignees used for the filter control */
  public assigneesControlOptions: MenuItem<UserResponse>[] = [];

  /** Filters to hand to the bill flag filters popover */
  public billFlagFilters!: BillFlagFilters;

  /** Entire view overlay */
  public viewStatus: Overlay = new Overlay('hasData');

  /** Grid overlay */
  public cardStatus: Overlay = new Overlay('hasData');

  /** List of rows selected in the grid, stored as their bill id */
  public selectedRows: number[] = [];

  /** Used to know if all pages in a paginated grid are selected by the user,
   *  this feature is currently on hold
   */
  public listSelected: boolean = false;

  /** Used to tell the flag actions menu that a single bill selected is held */
  public selectedBillIsHeld: boolean = false;

  /** Options to hand to the flag actions menu */
  public flagEntities?: FlagActionEntities

    /** Options to hand to the flag list download menu */
  public flagListDownloadEntities?: FlagListDownloadEntities;

  /** Current users information, used for the global currency symbol */
  public user!: Me;

  /** Callback to hand to the flag actions menu so it can save filters prior
   *  to bulk list actions
   */
  public preBulkActionCallback:() => Promise<void> = this.preBulkUpdateAction.bind(this);

  public removedFilters?: Tag[];

  /** Stored response for the last filter set */
  private listResponse!: ListResponse;

  /** Used in the bill filters popover to get data field ids */
  private getListFilters!: Observable<FilterResponse[]>;

  /** Used to know if the form groups valueChanges observable has been
   *  subscribed.  When switching in the tree and not changing entity types
   *  init is called but we do not want multiple subscriptions
   */
  private valueChangesSubscription!: Subscription;

  /** Used to know when we are resetting filters, this tells the valueChanges
   *  subscription to not fire until all filters are reset
   */
  private resettingFilters: boolean = false;

  /** Used to know when we are going through init for the first time, we only
   *  want to call flag types and assignees and do other setup once.  The Flag list filter
   *  service also relies on this.
   */
  private isFirstInit: boolean = true;

  /** Last filter set sent to the list service, used to know if we need
   *  to recall the list.  If you change an operator on a control without a value
   *  the valueChanges is fired, but no filter is produced.  This helps us identify that case.
   */
  private previousFilterSet: string = "";

   /**
   * Reference to the child kendo grid
   *
   * @type {GridComponent}
   */
  @ViewChild('grid', { static: true }) grid!: GridComponent;

  constructor(
    private listApi: ListService,
    private dialogService: DialogService,
    private flagListFilterService: FlagListFilterService,
    private translateService: TranslateService,
    private billViewPagerService: BillViewPagerService,
    private errorService: ErrorService,
    private flagHelperService: FlagHelperService,
    private windowService: WindowService,
    injector: Injector
  ) {
    super(injector);
  }

  protected async onInit(): Promise<void> {
    this.viewStatus.setStatus('pending', 'Loading');
    this.resetGridDefaults();

    let getListDataPromise = this.getListData();
    let promises: Promise<any>[]= [
      getListDataPromise
    ];

    // No need to get these more than once
    if (this.isFirstInit) {
      promises.push(this.getUser());
      promises.push(this.loadFlagTypes());
      promises.push(this.loadAssigneeOptions());
    }

    await Promise.all(promises);

    // First time in refresh the flag specific filters with
    // what is stored in cache
    if (this.isFirstInit) {
      let response = await getListDataPromise;
      this.syncFlagFiltersWithStore(response.filters);
      this.previousFilterSet = JSON.stringify(response.filters);

      // Store the currently set filters so we don't lose the ones coming from the widget
      await this.setFlagFilters();
      await this.flagListFilterService.setBillFlagFilters(this.billFlagFilters);

      this.removedFilters = response.removedFilterTags;

      this.isFirstInit = false;
    }

    this.viewStatus.setStatus("hasData");

    // Store the subscription once set so we don't resubscribe as the user
    // changes nodes in the tree
    if (!this.valueChangesSubscription) {
      this.valueChangesSubscription = this.formGroup.valueChanges.pipe(
        filter(() => !this.resettingFilters),
        debounceTime(300),
        takeUntil(this.destroyed)
      ).subscribe(async () => {
        this.resetGridDefaults();
        await this.setFlagFilters();
        await this.getListDataIfFiltersChanged();
      });
    }
  }

  /** Re-initialize the component when url changes */
  protected onNavigationEnd(): void {
    this.onInit();
  }

  /** Re-initialize the component when Ng1StateService emits a reloaded event */
  protected onReload(): void {
    this.onInit();
  }

  /** Display a message to the user if the download failed
   *  TODO: When action feedback is added add a message here
   */
  public flagListDownloadFailed(): void {

  }

  public openBill(event: MouseEvent, flag: FlagRow) {
    event.stopImmediatePropagation();

    this.billViewPagerService.setPagerContext({
      currentPageIds: this.gridData.data.map(row => { return row.billId; }),
      listId: this.listResponse.listId!
    });

    this.router.navigate(['bills/bill', flag.billId], { queryParams: { listId: this.listResponse.listId } });
  }

  /** Called when the page number is clicked on in the kendo grid */
  public pageChange(event: PageChangeEvent): void {
    // If you click on the page number you are currently on this event fires even though we didn't actually change anything
    // Just return in that case
    if (this.skip === event.skip && this.pageSize === event.take) {
      return;
    }

    this.skip = event.skip;
    this.pageSize = event.take;
    this.getListData();
  }

  /** Called when a kendo grid header is clicked to change the sort column/direction */
  public async sortChange(event: SortDescriptor[]): Promise<void> {
    this.sort = event;
    if (this.listResponse && this.listResponse.field) {
      let editColumns: ListColumnEdit[] = [];

      // Start with a reset state to clear any previous columns sort since we only
      // support single column sort.  We can only update fields that have a display order
      // greater than zero
      editColumns = this.listResponse.field.filter(field => {
        return field.displayOrder && field.displayOrder > 0;
      }).map(field => {
        let editColumn: ListColumnEdit = {
          displayOrder: field.displayOrder!,
          fieldId: field.fieldId!,
          sortDirection: "asc",
          sortOrder: 0,
          visible: field.visible!,
          width: field.width!
        };

        return editColumn;
      });

      if (this.sort.length && this.sort[0].dir !== undefined) {
        let foundField = this.columnDetails[this.sort[0].field];

        if (foundField) {
          // Now find the edit column using that fieldId
          let foundEditColumn = editColumns.find(editColumn => {
            return editColumn.fieldId === foundField!.fieldId;
          });

          if (foundEditColumn) {
            // Update the edit column's sort bits
            foundEditColumn.sortOrder = 1;
            foundEditColumn.sortDirection = this.sort[0].dir!;
          }
        }
      }

      if (this.listResponse && this.listResponse.listId) {
        await this.listApi.editListColumns(this.listResponse.listId, editColumns).toPromise();
      }

      this.getListData();
    }
  }

  /** Called when a row is selected in the kendo grid */
  public selectedRowsChange(event: SelectionEvent): void {
    this.listSelected = false;

    if (this.selectedRows.length) {
      this.flagEntities = { type: 'ids', ids: this.selectedRows};
      this.flagListDownloadEntities = {type: 'bills', billIds: this.selectedRows, listId: this.listResponse.listId!};

      if (this.selectedRows.length === 1) {
        let foundSelectedRow = this.gridData.data.find(row => {
          return row.billId === this.selectedRows[0];
        });

        this.selectedBillIsHeld = foundSelectedRow && foundSelectedRow.exportHold;
      }
    } else {
      this.flagEntities = undefined;
      this.flagListDownloadEntities = {type: 'list', listId: this.listResponse.listId!};
    }
  }

  /** Called by the flag actions menu after the manage flags dialog completes */
  public flagsUpdated(event: ManageFlagDialogContext): void {
    // Update our flag type list in case the user added one within the
    // manage flag dialog
    this.loadFlagTypes();

    this.resetGridDefaults();

    this.getListData();
    this.selectedRows = [];

    this.flagHelperService.updateSubject.next();
  }

  /** Opens the bill specific dialog to set filters */
  public showMoreFilters(): void {
    const context = new FlagListFiltersContext<BillFlagFilters>(this.billFlagFilters);

    context.getListFilters = this.getListFilters;
    context.entityFiltersChanged.pipe(
      debounceTime(300)
    ).subscribe(async filters => {
      this.resetGridDefaults();
      this.billFlagFilters = filters;
      await this.flagListFilterService.setBillFlagFilters(this.billFlagFilters);
      await this.getListDataIfFiltersChanged();
    });

    this.dialogService.openDialog(BillFlagListFiltersComponent, 'xsmall', context, {displayAsPanel: true}).pipe(tap(() => context.entityFiltersChanged.unsubscribe()))
  }

  /** Click handler on the filter tags for bill specific filters */
  public async removeFilterTag(filter: Filter): Promise<void> {
    this.resetGridDefaults();
    await this.clearBillListFilter(filter);
    await this.getListDataIfFiltersChanged();
  }

  /** Resets all filters, flag and bill specific */
  public async resetFilters(): Promise<void> {
    this.resetGridDefaults();

    // Reset bill filters
    for(let key in this.billFlagFilters){
      await this.clearBillListFilter(this.billFlagFilters[key]);
    }
    await this.flagListFilterService.setBillFlagFilters(this.billFlagFilters);

    // Set resetting filters flag so that our valueChanges subscription does not fire
    // when resetting the form group
    this.resettingFilters = true;
    this.formGroup.reset();

    await this.setFlagFilters();
    await this.getListDataIfFiltersChanged();

    this.resettingFilters = false;
  }

  /** Called before attempting a bulk action, this must resolve before any can proceed
   *  Exceptions thrown within are to be handled by the caller
   */
  public async preBulkUpdateAction(): Promise<void> {
    let response = await this.flagListFilterService.getFilters(false);
    let listFilters = await this.getListFilters.toPromise();

    let filtersToSave = this.flagListFilterService.getListFiltersToSave(response.filters, listFilters);

    await this.listApi.editListFilters(this.listResponse.listId!, filtersToSave).toPromise();
  }

  /** Used to clean up any leftover grid state when switching filters or like entities
   *  where we don't do a full destroy of the component
   */
  private resetGridDefaults(): void {
    // Resets to the first page
    this.skip = 0;
  }

  /** Sets the list to show the entire list is selected, this also sends the list select
   *  to the flag actions menu
   */
  public selectList() {
    this.listSelected = true;
    this.flagEntities = {type: 'list', ids: [this.listResponse!.listId!], totalCount: this.listResponse.meta!.totalCount};
    this.flagListDownloadEntities = {type: 'all', listId: this.listResponse!.listId!};
    this.selectedRows = this.gridData.data.map(row => row.billId);
  }



  /** Checks that the filter string returned from the service has changed from the previous call
   *  We have change watchers on the bill and flag filters form groups and operator changes can trigger
   *  them to fire, we do not want to call the api if the user changes an operator but didn't supply a value
   */
  private async getListDataIfFiltersChanged(): Promise<void> {
    let response = await this.flagListFilterService.getFilters(false);
    let filterJson = JSON.stringify(response.filters);

    if (filterJson !== this.previousFilterSet) {
      this.previousFilterSet = filterJson;
      this.getListData();
    }
  }

  /** Clears an individual bill list filter */
  private async clearBillListFilter(filter: Filter): Promise<void> {
    filter.value = null;
    await this.flagListFilterService.setBillFlagFilters(this.billFlagFilters);
  }

  /**Reloads the list using the currently set filters and page information
   * @returns the filters that were actually used to calculate the list
   *          so that we can render the correct filters in the pill row
   */
  private async getListData(): Promise<QuickFiltersWithError> {
    this.cardStatus.setStatus('pending');

    let flags: FlagRow[] = [];
    let totalNumberOfRecords: number = 0;

    let pageNumber = this.skip / this.pageSize + 1;
    let queryString = `pageSize=${this.pageSize}&pageNumber=${pageNumber}`;

    // Kendo will maintain previously selected rows on other pages if we allow them to
    // We've decided to let the user handle one page at a time, so we will clear for any
    // action that redraws the list
    this.selectedRows = [];
    const response = await this.flagListFilterService.getFilters(this.isFirstInit);
    const options: DynamicListRequest = {
      filters: response.filters,
      resetColumns: false
    };

    if (this.isFirstInit) {
      // Remove query params after the filters have been extracted from it
      // this way if the user changes filters manually, then views a bill and comes back, the manual changes
      // that are stored in the session storage won't get squashed by the ones on the widget from when they linked in
      await this.windowService.modifyHistoryQueryParamsSubset({dashboardId: null, widgetId: null, flagStatus: null});
    }

    try {
      // TODO: do we have enum for ListTypeId's. If not we should.
      let apiResponse = await this.listApi.getDynamicListWithDataWithHttpInfo(30, this.pageSize, pageNumber, options).toPromise();

      if (apiResponse.body) {
        this.listResponse = apiResponse.body;

        this.flagListDownloadEntities = {type: 'list', listId: this.listResponse.listId!};

        this.pageSize = Number(this.getHeaderValue(apiResponse.headers, "pagesize") || 50);
        totalNumberOfRecords = Number(this.getHeaderValue(apiResponse.headers, "totalnumberofrecords") || 0);

        this.setPaginationControls(totalNumberOfRecords);

        this.setListSort();

        flags = this.getListRows();

        await this.buildBillFlagFilters(options.filters!);

        if (this.isFirstInit) {
          // Only setting column details on the first load so if the user adjusts column widths
          // we will not lose them until we persist.
          this.setColumnDetails();

          this.getListFilters = this.listApi.getListFilters(this.listResponse.listId!).pipe(
            publishReplay(1),
            refCount()
          );
        }
      }

      this.gridData = {
        data: flags,
        total: totalNumberOfRecords
      };

      this.cardStatus.setStatus('hasData');
    } catch (error) {
      this.cardStatus.setStatus('error');
      this.cardStatus.message = this.errorService.parseApiError(error);
    }

    return response;
  }

  /** Loops through the list response fields and creates key/value object of each field for
   *  easy reference by the formatted field name
   */
  private setColumnDetails(): void {
    if (this.listResponse.field) {
      this.listResponse.field!.forEach(field => {
        if (field.caption) {
          this.columnDetails[this.formatFieldName(field.caption)] = field;
        }
      });
    }
  }

  /** Formats and returns a field name camel case */
  private formatFieldName(name: string): string {
    return camelCase(name);
  }

  /** Returns a response header value if it can be found */
  private getHeaderValue(headers: HttpHeaders, key: string): any {
    if (headers.get(key)) {
      return headers.get(key);
    }
    return null;
  }

  /** Sets the kendo grid pagination controls */
  private setPaginationControls(totalNumberOfRecords: number): void {
    this.pageable = {
      buttonCount: 5,
      info: true,
      type: 'numeric',
      previousNext: false
    }

    // Hiding the "1" button when there is only a single page
    if (totalNumberOfRecords <= this.pageSize) {
      this.pageable.buttonCount = 0;
    }
  }

  /** Sets the sort order and direction indicator for the kendo grid */
  private setListSort(): void {
    if (this.listResponse.field) {
      let foundSortField = this.listResponse.field.find(field => {
        return field.sortOrder === 1;
      });

      if (foundSortField) {
        this.sort = [
          {
            dir: foundSortField.sortDirection! === "asc" ? "asc" : "desc",
            field: this.formatFieldName(foundSortField.caption!)
          }
        ]
      }
    }
  }

  /** Gets the row data formatted for the kendo grid */
  private getListRows(): FlagRow[] {
    let rows: FlagRow[] = [];

    if (this.listResponse.data && this.listResponse.data.length) {
      rows = this.listResponse.data.map(fields => {
        const flag: FlagRow = {};
        fields.forEach(field => flag[this.formatFieldName(field.name!)] = field.value);
        return flag;
      });
    }

    return rows;
  }

  /** Gets the flag type options for the combobox */
  private async loadFlagTypes(): Promise<void> {
    this.flagTypeOptions = await this.flagHelperService.getFlagTypeOptions(true);
  }

  /** Gets the list of assignees for the combobox */
  private async loadAssigneeOptions(): Promise<void> {
    const defaultOptions: MenuItem<any>[] = [
      {label: this.translateService.instant('Unassigned_TC'), value: 'unassigned'},{label: this.translateService.instant('AssignedToMe_SC'), value: 'me', display: 'divider'}
    ];
    const users = await this.flagHelperService.getAssigneeOptions();
    this.assigneesControlOptions = [...defaultOptions, ...users];
  }

  /** Gets the current user for their currency format */
  private async getUser(): Promise<void> {
    this.user = await this.userService.getUser().toPromise();
  }

  /** Gets the filters from our service store and patches the values to our
   *  form group
   */
  private syncFlagFiltersWithStore(filters: QuickFilter[]): void {
    let patch: FlagFiltersFormValue = this.formGroup.getDefaultValue();

    filters.forEach(filter => {
      switch (filter.caption) {
        case 'Flag Status': {
          patch.flagStatus = filter.value === '1';
          break;
        }
        case 'Flag Type Name': {
          patch.flagType = this.flagTypeOptions.find(flagType => { return flagType.value.flagTypeInfo === filter.value; }) || null;
          break;
        }
        case 'Cost Recovery': {
          patch.costRecovery.value = Number(filter.value);
          patch.costRecovery.operator = filter.operator || this.costRecoveryOperators[0].value;
          break;
        }
        case 'Flag Assigned To Me': {
          patch.assignee = this.assigneesControlOptions.find(assignee => { return (<any>assignee.value) === 'me'}) || null;
          break;
        }
        case 'Flag Assigned To': {
          patch.assignee = this.assigneesControlOptions.find(assignee => { return (<UserResponse>assignee.value).fullName === filter.value}) || null;
          break;
        }
        case 'Flag Unassigned': {
          patch.assignee = this.assigneesControlOptions.find(assignee => { return (<any>assignee.value) === 'unassigned'}) || null;
          break;
        }
      }
    });

    this.formGroup.patchValue(patch);
  }

  /** Sets the form groups filters to the service store */
  private async setFlagFilters(): Promise<void> {
    let formValue = this.formGroup.value;

    let filters: FlagFilters = <FlagFilters>{
      flagStatus: {
        caption: "Flag Status",
        operator: "equals",
        // True is resolved
        value: formValue.flagStatus ? '1' : '0'
      },
      flagType: <Filter>{
        caption: "Flag Type Name",
        operator: "equals",
        value: formValue.flagType && formValue.flagType.value ? formValue.flagType.value.flagTypeInfo : null
      },

      costRecovery: <Filter>{
        caption: "Cost Recovery",
        operator: formValue.costRecovery.operator || null,
        value: formValue.costRecovery.value !== null && formValue.costRecovery.value !== undefined ? formValue.costRecovery.value : null
      }
    }

    if (formValue.assignee && formValue.assignee.value) {
      if (formValue.assignee.value === "me") {
        filters.assignees = <Filter>{
          caption: "Flag Assigned To Me",
          operator: "equals",
          value: '1'
        }
      } else if (formValue.assignee.value === "unassigned") {
        filters.assignees = <Filter>{
          caption: "Flag Unassigned",
          operator: "equals",
          value: '1'
        }
      } else {
        filters.assignees = <Filter>{
          caption: "Flag Assigned To",
          operator: "equals",
          value: formValue.assignee && formValue.assignee.value ? (<UserResponse>formValue.assignee.value).fullName : null
        }
      }
    }

    await this.flagListFilterService.setFlagFilters(filters);
  }

  /** Builds the bill specific list of filters and sends to the service to be
   *  used by the bill flag filters dialog
   */
  private async buildBillFlagFilters(filters: QuickFilter[]): Promise<void> {
    // TODO: API should be changed to return the specific subset (the union of columns and filter fields)
    const accountNameFilter = this.listResponse.field!.find(f => f.caption === 'Account Name')!;
    const accountCodeFilter = this.listResponse.field!.find(f => f.caption === 'Account Code')!;

    const vendorNameFilter = this.listResponse.field!.find(f => f.caption === 'Vendor Name')!;
    const vendorCodeFilter = this.listResponse.field!.find(f => f.caption === 'Vendor Code')!;

    const billCostFilter = this.listResponse.field!.find(f => f.caption === 'Total Cost')!;
    const billStartFilter = this.listResponse.field!.find(f => f.caption === 'Bill Begin Date')!;
    const billEndFilter = this.listResponse.field!.find(f => f.caption === 'Bill End Date')!;
    const billCreatedFilter = this.listResponse.field!.find(f => f.caption === 'Bill Entry Date')!;

    const billExportHoldFilter = this.listResponse.field!.find(f => f.caption === 'Bill Export Hold')!;

    let billFlagFilters: BillFlagFilters = {
      accountCode: {
        fieldId: accountCodeFilter.fieldId!,
        caption: accountCodeFilter.caption!,
        operator: 'equals',
        value: null
      },
      accountName: {
        fieldId: accountNameFilter.fieldId!,
        caption: accountNameFilter.caption!,
        operator: 'equals',
        value: null
      },
      vendorCode: {
        fieldId: vendorCodeFilter.fieldId!,
        caption: vendorCodeFilter.caption!,
        operator: 'equals',
        value: null
      },
      vendorName: {
        fieldId: vendorNameFilter.fieldId!,
        caption: vendorNameFilter.caption!,
        operator: 'equals',
        value: null
      },
      billCost: {
        fieldId: billCostFilter.fieldId!,
        caption: billCostFilter.caption!,
        operator: 'greater than equal',
        value: null
      },
      billStart: {
        fieldId: billStartFilter.fieldId!,
        caption: billStartFilter.caption!,
        operator: 'greater than',
        value: null
      },
      billEnd: {
        fieldId: billEndFilter.fieldId!,
        caption: billEndFilter.caption!,
        operator: 'greater than',
        value: null
      },
      billCreated: {
        fieldId: billCreatedFilter.fieldId!,
        caption: billCreatedFilter.caption!,
        operator: 'greater than',
        value: null
      },
      billExportHold: {
        fieldId: billExportHoldFilter.fieldId!,
        caption: billExportHoldFilter.caption!,
        operator: 'equals',
        value: null
      }
    };

    await this.flagListFilterService.restoreBillFlagFilters(billFlagFilters, filters);
    this.billFlagFilters = billFlagFilters;
  }

  /** Save the kendo column widths to the API */
  public async persistColumnWidths() {
      if (this.listResponse && this.listResponse.field && this.listResponse.listId) {
        //Get the columns whose field property is found in columnDetails (i.e. columns whose fields came from the API)
        const kendoColumns: ColumnBase[] = this.grid.columns.toArray().filter(column => {
          return (column instanceof ColumnComponent && (column as ColumnComponent).field && this.columnDetails[(column as ColumnComponent).field]);
      });

      //Create a map that maps the field name of each kendo column to the width of that column.
      //Because of the filter above, we know every column in kendoColumns is an instance of ColumnComponent, so casting is safe.
      const columnWidthMap: { [key: string]: number } = {};
      kendoColumns.forEach(column => {
        const castedColumn: ColumnComponent = (column as ColumnComponent);
        columnWidthMap[castedColumn.field] = Math.floor(castedColumn.width);
      });

      //For each field returned by the API, compare its width to the width of the kendo column that corresponds to that field.
      //If the width changed, create a DTO to change to the new width.
      //Note: if no column widths have ever been changed, the API will return a width of 1 for all fields, which means the width of the corresponding kendo column will be undefined (because the template
      //  ignores widths of 1). Hence, we want to filter out any fields with undefined kendo column widths because their widths are unchanged.
      const editColumns: ListColumnEdit[] = this.listResponse.field.filter(field => {
        const fieldName = this.formatFieldName(field.caption!);
        //Filter out fields with undefined width (i.e. when no column widths have ever been changed) and fields whose widths haven't changed since last time.
        return field.displayOrder && (field.displayOrder > 0) && columnWidthMap[fieldName] && (columnWidthMap[fieldName] !== this.columnDetails[fieldName].width!);
      }).map(field => {
        let editColumn: ListColumnEdit = {
          displayOrder: field.displayOrder!,
          fieldId: field.fieldId!,
          sortDirection: field.sortDirection!,
          sortOrder: field.sortOrder!,
          visible: field.visible!,
          width: columnWidthMap[this.formatFieldName(field.caption!)] === 1 ? 2 : columnWidthMap[this.formatFieldName(field.caption!)]  //if they somehow set a width to 1, we want to send back 2 (because 1 will be ignored in the template)
        };
        return editColumn;
      });

      if (editColumns.length > 0) {
        try {
          await this.listApi.editListColumns(this.listResponse.listId, editColumns).toPromise();
        } catch(e) {
          this.errorService.logConsoleError(e);
        }
      }

     }
  }

}
