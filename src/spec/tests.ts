import * as fs from 'fs';
import { SpecWriter } from '../SpecWriter';

/**
 * To add a test case:
 * 	add the file you want to test to the cases directory (make sure to change the extension to .txt)
 *  add the expected output to the expectations directory (extension should also be .txt)
 * 	your case will automatically be added to the test suite
 * Note:
 * 	make sure your expected output has removed any import declarations, since those aren't added by the SpecWriter
 */
describe('tests', ()=> {
	let cases: string[] = fs.readdirSync('./src/spec/cases').map(fileName => {
		const split = fileName.split('.');
		split.pop();
		return split.join('.');
	});
	cases.forEach(test => {
		it(`generates expected spec for ${test}`, ()=> {
			const path = `./src/spec/cases/${test}.txt`;
			const specPath = `./src/spec/expectations/${test}.spec.txt`;
			const outputPath = './src/spec/output/output.txt';
	
			const file = fs.readFileSync(path, 'utf-8');
			fs.writeFileSync(outputPath, new SpecWriter().generateSpec(file));
			let expectedSpec = fs.readFileSync(specPath, 'utf-8');
			let output = fs.readFileSync(outputPath, 'utf-8');
			//replace any occurence of \r\n with \n. for some reason the expectations had \r\n (probably added by windows)
			expectedSpec = expectedSpec.replace(/(?:\r\n)/g, '\n').trim();
			output = output.replace(/(?:\r\n)/g, '\n').trim();
			expect(expectedSpec).toEqual(output);
		});
	});
});