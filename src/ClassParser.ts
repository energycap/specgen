export type ClassType = 'component' | 'service' | 'pipe' | 'directive';
export type ParentClass ='PageBaseComponent' | 'FormBaseComponent' | 'ViewBaseComponent' | 'SearcherBaseDirective' | '';

export class ClassParser {
	public className!: string;
	public classType!: ClassType;
	public parentClass: ParentClass = '';
	public dependencies: string[] = [];
   
	private reducedFile!: string;

  constructor(private file: string) {
		this.getReducedFile();
		this.parseNameAndParent();
		this.parseDependencies();
	}

  //Remove everything before and including the decorator to make parsing easier.
	private getReducedFile() {
		const decorator = ['@Component', '@Pipe', '@Injectable', '@Directive'].filter(d => this.file.indexOf(d) > -1)[0];
		this.classType = (decorator === '@Injectable') ? 'service' : <ClassType>decorator.substring(1).toLowerCase();
		const start = this.file.indexOf(decorator);
		const end = this.file.indexOf(')', start+1);
		this.reducedFile = this.file.substring(end + 1);
		//If the class is a directive, it may have a @Host() decorator in its constructor, which will break parseDependencies() without this
		this.reducedFile = this.reducedFile.replace('@Host()', '');
	}

	//Get the name of the class and its parent from the class signature.
	private parseNameAndParent() {
		const endClassSignature = this.reducedFile.indexOf('{'); 
		const pieces = this.reducedFile.substring(0, endClassSignature).split(' ').map(s => s.trim());
		const classIndex = pieces.findIndex(p => p === 'class');
		this.className = pieces[classIndex + 1];
		const extendsIndex = pieces.findIndex(p => p === 'extends');
		if(extendsIndex > -1) {
			if(['PageBaseComponent', 'FormBaseComponent', 'ViewBaseComponent', 'SearcherBaseDirective'].includes(pieces[extendsIndex + 1])) {
				this.parentClass = <ParentClass>pieces[extendsIndex + 1];
			}
		}
	}

	//Get all dependencies from the constructor.
	private parseDependencies() {
		const search = 'constructor(';
		const constructorStart = this.reducedFile.indexOf(search);
		const constructorEnd: number = this.reducedFile.indexOf(')', constructorStart+1);
		let sub = this.reducedFile.substring(constructorStart, constructorEnd);
		sub = sub.substring(search.length); //remove 'constructor('
		this.dependencies = sub.split(',').map(s => s.substring(s.indexOf(':')+1).trim()).filter(s => s !== '');
	} 
}