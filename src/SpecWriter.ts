import { ClassParser } from "./ClassParser";

type InjectorSource = 'ComponentTestInjectorFactory.getFormBaseComponentInjector()' |
                      'ComponentTestInjectorFactory.getPageBaseComponentInjector()' |
                      'ComponentTestInjectorFactory.getViewBaseComponentInjector()' |
                      'SearcherBaseMocks.getSearcherBaseInjector()' |
                      '';
export class SpecWriter {
	private parser!: ClassParser;
	private injectorSource: InjectorSource = '';
	private declarations!: string;
	private assignments!: string;

	constructor() {}

	public generateSpec(file: string): string {
		this.init(file);
//yes I'm aware this looks like garbage
return `
describe('${this.parser.className}', () => {
${this.declarations}
\tbeforeEach(() => {
${this.assignments}
\t});
});
`;
	}
    
	private init(file: string) {
		this.parser = new ClassParser(file);
		if(this.parser.parentClass) {
			this.getInjectorSource();
		}
		this.getDeclarations();
		this.getAssignments();
	}

	private getInjectorSource() {
		switch(this.parser.parentClass) {
			case 'FormBaseComponent':
				this.injectorSource = 'ComponentTestInjectorFactory.getFormBaseComponentInjector()';
				return;
			case 'PageBaseComponent':
				this.injectorSource = 'ComponentTestInjectorFactory.getPageBaseComponentInjector()';
				return;
			case 'ViewBaseComponent':
				this.injectorSource = 'ComponentTestInjectorFactory.getViewBaseComponentInjector()';
				return;
			case 'SearcherBaseDirective':
				this.injectorSource = 'SearcherBaseMocks.getSearcherBaseInjector()';
				break;
		}
	}

	private getDeclarations() {
		const lines: string[] = [];
		lines.push(`let ${this.parser.classType}: ${this.parser.className};`);
		if(this.parser.parentClass === 'FormBaseComponent') {
			lines.push(`let helper: ComponentTestHelper<${this.parser.className}>;`);
		} else if(this.parser.parentClass === 'PageBaseComponent') {
			lines.push(`let helper: PageBaseComponentTestHelper<${this.parser.className}>;`);
		}
		this.parser.dependencies.forEach(dep => {
			if(dep === 'Injector') {
				lines.push(`let injector: Injector;`);
			} else if(dep === 'TranslateService') {
				lines.push(`let mockTranslateService: MockTranslateService;`);
			} else if(dep === 'ComboboxComponent') {
				lines.push(`let mockComboboxComponent: ComboboxComponent;`);
			} else {
				lines.push(`let mock${dep}: jasmine.SpyObj<${dep}>;`);
			}
		});
		this.declarations = lines.map(line => '\t' + line).join('\n') + '\n';
	}

	private getAssignments() {
		const lines: string[] = [];
		this.parser.dependencies.forEach(dep => {
			if(dep !== 'Injector') {
				if(dep === 'TranslateService') {
					lines.push(`mockTranslateService = new MockTranslateService('_T');`);
				} else if(dep === 'ComboboxComponent') {
					lines.push(`mockComboboxComponent = SearcherBaseMocks.getMockComboboxComponent();`);
				} else {
					lines.push(`mock${dep} = SpyFactory.createSpy(${dep});`);
				}
			} else if(dep === 'Injector' && this.parser.parentClass) {
				lines.push(`injector = ${this.injectorSource};`);
			}
		});
		lines.push(`${this.parser.classType} = new ${this.parser.className}${this.getConstructor()};`);
		if(this.parser.parentClass === 'FormBaseComponent') {
			lines.push(`helper = new ComponentTestHelper(component);`);
		} else if(this.parser.parentClass === 'PageBaseComponent') {
			lines.push(`helper = new PageBaseComponentTestHelper(component, injector);`);
		}
		this.assignments = lines.map(line => '\t\t' + line).join('\n');
	}

	private getConstructor() : string {
		return `(${this.parser.dependencies.map(dep => {
			if(dep === 'Injector') {
				return 'injector';
			} else if(dep === 'TranslateService') {
				return '<TranslateService>mockTranslateService';
			} 
			return `mock${dep}`;
		}).join(', ')})`;
	}
}