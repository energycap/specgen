import * as fs from 'fs';
import { Uri, window } from 'vscode';
import { ExtensionContext } from 'vscode';
import { commands } from 'vscode';
import { SpecWriter } from './SpecWriter';
//publishing: https://code.visualstudio.com/api/working-with-extensions/publishing-extension
export function activate(context: ExtensionContext) {
	//When a 'Generate Spec' is clicked for a file in the tree, this gets executed with the uri argument pointing
	//to the file they clicked.
	commands.registerCommand('ecap-specgen.writeSpec', async (uri: Uri) => {
		const file = fs.readFileSync(uri.fsPath, 'utf-8');
		const specFilePath = uri.fsPath.split('.').map(part => part === 'ts' ? 'spec.ts' : part).join('.');
		//write the spec file
		fs.writeFileSync(specFilePath, new SpecWriter().generateSpec(file));
		//open the spec file in the editor
		await commands.executeCommand('vscode.open', Uri.file(specFilePath));
		//add imports 
		window.showInformationMessage('Adding imports. This may take a few seconds.');
		await commands.executeCommand('_typescript.applyFixAllCodeAction', specFilePath, {fixId: 'fixMissingImport'});
	});
}